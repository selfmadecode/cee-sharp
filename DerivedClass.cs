﻿using System;

namespace practice
{
    class DerivedClass
    {
        public class Position
        {
            public int x { get; set; }
            public int y { get; set; }
        }
        public class Size
        {
            public int Width { get; set; }
            public int Height { get; set; }
        }
        public class Shape
        {
            public Position position { get; } = new Position(); // instantiating the position class
            public Size size { get; } = new Size();

            public virtual void Draw() =>
            Console.WriteLine($"Shape with {position.x}, {position.y} and {size.Width}, {size.Height}");
            // a virtual function can be overrided by a derived class using the OVERRIDE Keyword

        }
        public class Rectangle : Shape
        {
            // overridding the draw method
            public override void Draw() =>
            Console.WriteLine($"Rectangle with {position.x}, {position.y} and {size.Width}, {size.Height}");
        }

        public abstract class AbstractShape
        {
            //An abstract class cannot be instantiated
            //Abstract class must have an abstract method
            //Abstarct methods have no body
            // Any class that inherits this class must implement all its abstract methods
            public abstract void Resize(int width, int height); // abstract method
            public abstract void Resize(string messsage, int height);
            public void Resize(string message)
            {
                Console.WriteLine(message);
            }
            //public abstract void Resize( int height)
            //{
            //    // Cannot declare a body because it is marked abstract
            //}
        }

        public class NonAbstarctClass : AbstractShape
        {
            public override void Resize(int width, int height)
            {
                // must implement the Abstract method
                throw new NotImplementedException();
            }

            public override void Resize(string messsage, int height)
            {
                throw new NotImplementedException();
            }
        }
    }
}
