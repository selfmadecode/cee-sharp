﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice.Services
{
    class Delegates
    {
        delegate string GetXToString();

        delegate double DoubleOp(double x);
        // return type for the delegate and its instance must match
        public void UsingDelegate()
        {
            int x = 30;
            GetXToString GetX = new GetXToString(x.ToString);
            // Console.WriteLine("The value of X is " + GetX.Invoke());
            Console.WriteLine("The value of X is " + GetX());
        }
        public static double MultiplyByTwo(double value) => value * 2;
        public static double Square(double value) => value * value;

        public static void StartHere()
        {
            DoubleOp[] mathOperation = {
            Delegates.MultiplyByTwo,
            Delegates.Square
            
            };

            for (int i = 0; i < mathOperation.Length; i++)
            {
                Console.WriteLine($"Using operations[{i}] :");
                ProcessAndDisplayNumber(mathOperation[i], 2.0);
                ProcessAndDisplayNumber(mathOperation[i], 7.94);
                ProcessAndDisplayNumber(mathOperation[i], 1.414);
                Console.WriteLine();
            }

        }
        static void ProcessAndDisplayNumber(DoubleOp action, double value)
        {
            double result = action(value);
            Console.WriteLine($"Value is {value}, result of operation is {result}");
        }

    public delegate double MathOperations(double x, double y);

        public static double Addition(double a, double b)
        {
            Console.WriteLine($"The value for the addition is {a + b} ");
            return a + b;
        }
        public static double Multiplication(double a, double b)
        {
            Console.WriteLine($"The value for the multiplication is {a * b} ");
            return a * b;
        }
    }
}
