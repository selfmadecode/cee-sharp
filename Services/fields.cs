﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice
{
    class fields
    {
        private static string FirstName; // cannot be assigned from another class expect this class
        public static string SecondName; // can be assigned value from any part of this program

        public static void ChangeUserName(string firstname, string secondname)
        {
            FirstName = firstname + "and ...............";
            SecondName = secondname;
            Console.WriteLine(FirstName + SecondName);
            Console.WriteLine(Environment.NewLine);
        }
        public static void ChangeUserName(string firstname, string secondname, int age)
        {
            FirstName = "__________________";
            SecondName = secondname;
            Console.WriteLine(FirstName + SecondName);
            Console.WriteLine(Environment.NewLine);
        }
        public static void getNames() => Console.WriteLine(FirstName + SecondName); // using single line
        

        // using property Age to assign value to the age field
        private static int age;
        public static int Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
            }
        }
        public static int GrandAge { get; set; } // auto implemented properties

        public static void getAge(int Myage)
        {
            Age = Myage;
            Console.WriteLine("Age is " + age);
            GrandAge = 1000;
            Console.WriteLine(GrandAge);
        }


        private string _firstName;
        public string MyFirstName
        {
            get => _firstName;
            set => _firstName = value;
        }

    }
}
