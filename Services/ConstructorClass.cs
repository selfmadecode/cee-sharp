﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice.Services
{
    class ConstructorClass
    {   // Page 90, Professional C#
        private double Length { get; }
        private double Width { get; }
        private double Diagonal;

        public ConstructorClass(int length, int width)
        {
            Length = length;
            Width = width;
        }

        public void GetDiagonal()
        {
            Diagonal = Length * Width;
            Console.WriteLine($"Diagonal is: {Diagonal} ");
        }




        private int _number;  

        //public ConstructorClass(int number)
        //{
        //    _number = number;
        //}

        //Expression Bodies with Constructors
        private static ConstructorClass s_instance;
        private int _state;
        private ConstructorClass(int state) => _state = state;
        public static ConstructorClass Instance =>
        s_instance ?? (s_instance = new ConstructorClass(42)); //Expression Bodies with Constructors

    }
}
