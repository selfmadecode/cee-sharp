﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace practice
{
    class OptionalArgument
    {
        public static void OptionalArg( string notOptionalArgument, string optionalArgument = "Sorry, this is the default value: No argument was passed", int anotherOptional = 30)
        {
            Console.WriteLine($"The first Argument is: {notOptionalArgument} and the second is: {optionalArgument}, another optional param is {anotherOptional}");

        }
        public static void VariableNumberOfParam(params int[] data)
        {
            // this method will print the whole paramenters passed to this method
            int[] dataArr = { };
            foreach (var item in data)
            {
                dataArr = dataArr.Append(item).ToArray(); // adding to the array
            }
           // Console.WriteLine("[{0}]", string.Join(",", dataArr));
            Console.WriteLine($"Using an array to print the following value: [{string.Join(", ", dataArr)}]");
            Console.WriteLine(Environment.NewLine);
        }
        public static void VariableNumberOfParam(params object [] data)
        {
            // will print all params pass to this method without checking the data type
            //int[] dataArr = { };
            List<object> dataArr = new List<object> { }; 
            foreach (var item in data)
            {
                dataArr.Add(item);
            }
            Console.WriteLine($"Using a list to print the following value: [{string.Join(", ", dataArr)}]"); //prints out elements of the list

        }
    }
}
