﻿using System;

namespace practice.Services
{
    class Interface
    {
        public void Explain()
        {
            Console.WriteLine("Part of this class, instantiate this class (interface) to see me");
            Console.WriteLine(@"Note that the other classes contained cannot be seen if 
                                the Interface class alone is instantiated
                                in order to see the first bank class, instantaite using interface.firsBank");
        }
        public interface IBankAccount
        {
            void PayIn(decimal amount);
            void CheckBalance();
            bool WithDraw(decimal amount);
            decimal Balance { get; }
        }


        public class FirstBank : IBankAccount
        {
            private decimal _balance;
            public decimal Balance => _balance;

            public void CheckBalance() => Console.WriteLine($"Your balance is: {_balance}");
            public override string ToString() => $"Your balance is: {_balance}";
            

            public void PayIn(decimal amount) // payIn
            {
                Console.WriteLine($"N{amount} deposited successful!!");
                _balance += amount;
            }

            public bool WithDraw(decimal amount) // Withdraw
            {
                if (amount <= _balance)
                {
                    _balance -= amount;
                    Console.WriteLine($"Withdrawal of N{amount} was successful!!");
                    return true;
                }
                Console.WriteLine("Insufficient balance");
                return false;
            }
        }
        public interface ITransferBankAccount : IBankAccount
        {
            //Because ITransferBankAccount is derived from IBankAccount, it gets all the members of IBankAccount as well as its own.

            bool TransferTo(IBankAccount destination, decimal amount);
        }

        // DIAMONBANK IMPLEMENTS THE INHERITED INTERFACE
        public class DiamondBank : ITransferBankAccount
        {
            private decimal _balance;
            public decimal Balance => _balance;

            public void CheckBalance() => Console.WriteLine($"Your balance is: {_balance}");
            public override string ToString() => $"Your balance is: {_balance}";


            public void PayIn(decimal amount) // payIn
            {
                Console.WriteLine($"N{amount} deposited successful!!");
                _balance += amount;
            }

            public bool WithDraw(decimal amount) // Withdraw
            {
                if (amount <= _balance)
                {
                    _balance -= amount;
                    Console.WriteLine($"Withdrawal of N{amount} was successful!!");
                    return true;
                }
                Console.WriteLine("Insufficient balance");
                return false;
            }

            public bool TransferTo(IBankAccount destination, decimal amount)
            {
                if (amount <= _balance)
                {
                    _balance -= amount;
                    destination.PayIn(amount); // the IBankAccount has a payin method
                    Console.WriteLine($"Transfer of N{amount} was successful!!");
                    return true;
                }
                Console.WriteLine("Insufficient balance");
                return false;
            }
        }
    }
}
