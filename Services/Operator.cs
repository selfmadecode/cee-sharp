﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace practice.Services
{
    class Operator
    {
        public int One, Two, Three;
        public Operator(int one, int two, int three)
        {
            this.One = one;
            this.Two = two;
            this.Three = three;

        }

        public override string ToString()
        {
            return $"Tha data are: {One}, {Two} and {Three}";
        }
        public void Operators()
        {
            //int bb = 1;
            //int aa = 1;
            //Console.WriteLine(bb++); // increments after evaluation
            //Console.WriteLine(++aa); // increment before evaluation
            //Console.WriteLine(aa);
            //Console.WriteLine(bb);

           // int NoVale = null; // cannot conver null to int, to use null value, you have to make the data type nullable
            int? NoValue = null;
            List<int> listVal = new List<int>();
            listVal.Add(One);
            listVal.Add(Two);
            listVal.Add(Three);

            foreach (var num in listVal)
            {
                Console.WriteLine(num.ToString());
            }

            
        }
       
    }
}
