﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice.Services
{
    class DateTimeClass
    {
        public static string myName;
        public static void TomorrowAndYesterday()
        {
            try
            {
                Console.WriteLine($"My name is {myName}");
                string enteredDate;
                DateTime tomorrow, yesterday;
                Console.Write("To get tomorrow and yesterday's date, Enter a date in DD MM YYYY format: "); // if month is the last input, this will fail
                enteredDate = Console.ReadLine();
                DateTime givendate = Convert.ToDateTime(enteredDate);
                tomorrow = givendate.AddDays(1); // adds a day
                yesterday = givendate.AddDays(-1); // subtracts a day
                Console.WriteLine($"Tomorrow is: {tomorrow.ToLongDateString()} and yesterday was {yesterday.ToShortDateString()}");
                Console.WriteLine(Environment.NewLine);
            }
            catch (Exception error)
            {
                Console.WriteLine(error.Message);
            }
            Console.WriteLine("Click any key...");
            Console.ReadLine();
        }

        public static void FirstAndLastDay()
        {
            try
            {
                string enteredDate;
                DateTime firstDay, lastDay;
                Console.Write("To get the first and last days of a month, enter a date in DD MM YYYY format: "); // if month is the last input, this will fail
                enteredDate = Console.ReadLine();
                DateTime givendate = Convert.ToDateTime(enteredDate);
                int year = givendate.Year;
                int month = givendate.Month;
                firstDay = new DateTime(year, month, 1); // creates a new date instance with the year and month supplied, 1 is the first day 
                lastDay = firstDay.AddMonths(1).AddDays(-1); // adds a month to the first day, subtract a day
                Console.WriteLine($"First day of the month is {firstDay.ToShortDateString()} and the last day is {lastDay.ToLongDateString()}");
                Console.WriteLine(Environment.NewLine);
            }
            catch (Exception)
            {

                Console.WriteLine("Incorrect date supplied!");
            }
            Console.ReadLine();

        }
    }
}
