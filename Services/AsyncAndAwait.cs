﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace practice.Services
{
    class AsyncAndAwait
    {
        private const string url = "http://www.cninnovation.com";

        public static async Task ThrowAfter(int ms, string message)
        {
            await Task.Delay(ms);
            Console.WriteLine("hereeeeeeeeee1");
            Console.WriteLine("hereeeeeeeeee2");
            Console.WriteLine("hereeeeeeeeee3");
            Console.WriteLine("hereeeeeeeeee4");
            Console.WriteLine("hereeeeeeeeee5");
            Console.WriteLine("hereeeeeeeeee6");
            Console.WriteLine("hereeeeeeeeee7");
            Console.WriteLine("hereeeeeeeeee8");
            Console.WriteLine("hereeeeeeeeee9");
            Console.WriteLine("hereeeeeeeeee");
            throw new Exception(message);
        }
        public static async void DontHandle()
        {
            try
            {
               await ThrowAfter(200, "first");
                // exception is not caught because this method is finished
                // before the exception is thrown
            }
            catch (Exception ex)
            {
                Console.WriteLine($"handled {ex.Message}");
            }
        }










        // synchronization method
        public static void SyncMethod()
        {
            // In the following code snippet,
           // DownloadString makes an HTTP request and writes the response in the string content
            using (var client = new WebClient())
            {
                Console.WriteLine(nameof(SyncMethod));
                string content = client.DownloadString(url);    
                Console.WriteLine(content.Substring(0, 100));
            }
            // The method DownloadString blocks the calling thread until the result is returned.It’s not a good idea to
            // invoke this method from the user interface thread of the client application because it blocks the user interface
        }
        public static void AsyncMethod()
        {
            Console.WriteLine(nameof(AsyncMethod));

            WebRequest request = WebRequest.Create(url);
            // webrequest returns an IAsyncResult that can be used to verify if the method is completed
            // The BeginGetResponse takes an async callback ReadResponse(delegate)
            IAsyncResult result = request.BeginGetResponse(ReadResponse, null);

            // the callback
            void ReadResponse(IAsyncResult ar)
            {
                using (WebResponse response = request.EndGetResponse(ar))
                {
                    Stream stream = response.GetResponseStream();
                    var reader = new StreamReader(stream);
                    string content = reader.ReadToEnd();
                    Console.WriteLine(content.Substring(0, 100));
                    Console.WriteLine();
                }
            }
        }
        public static async Task TaskBasedAsyncPatternAsync()
        {
            // the synchonous method was updated to use a TaskBasedAsyncPattern that returns a task
            // and the type of the task, in this case 'string'

            using (WebClient client = new WebClient())
            {
                string content = await client.DownloadStringTaskAsync(url);
                Console.WriteLine(content.Substring(0, 100));
            }
        }
        public static void TaskAndThreadInfo(string info)
        {
            // method is created to write thread and task information to the console
            //Task.CurrentId returns the identifer of the task. Thread.CurrentThread.ManagedThreadId returns the identifer of the current thread
            Console.WriteLine(nameof(TaskAndThreadInfo));
            Console.WriteLine("Checker: this TaskAndThread method that checks for thread and task");
            string taskInfo = Task.CurrentId == null ? "No Task" : "Task " + Task.CurrentId;
            Console.WriteLine($"{info} in thread {Thread.CurrentThread.ManagedThreadId} " + $"and {taskInfo}");
            Console.WriteLine("Leaving Checker");
        }
        public static string Greeting(string name)
        {
            Console.WriteLine("Greeting has been callled");
            Console.WriteLine("Greeting is about to call Checker");
            TaskAndThreadInfo($"running {nameof(Greeting)}");
            Console.WriteLine("The waiting guy");
            Console.WriteLine("Delay about to happen");
            Task.Delay(3000).Wait();
            return $"Hello, {name}";
        }
        static Task<string> GreetingAsync(string name) =>
        Task.Run<string>(() =>
        {
            Console.WriteLine("The arrow guy");
            Console.WriteLine("The main guy called this arrow guy, Greeting Asyn");
            Console.WriteLine("Arrow guy calling checker");
            TaskAndThreadInfo($"running {nameof(GreetingAsync)}");
            Console.WriteLine("The arrow guy about to call Greeting");
            return Greeting(name);
        });
        public async static void CallerWithAsync()
        {
            Console.WriteLine("1. This is the main caller");
            TaskAndThreadInfo($"started {nameof(CallerWithAsync)}");
            Console.WriteLine("The main guy is about to call GreetingAsync and await the resul");
            string result = await GreetingAsync("Stephanie");
            Console.WriteLine(result);
            Console.WriteLine("The main guy about to call the Checker again");
            TaskAndThreadInfo($"ended {nameof(CallerWithAsync)}");
        }
    }
}
