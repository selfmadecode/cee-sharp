﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice.Services
{
    class ExceptionHandling
    {
        public static void TryCatch()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("Type a number between 0 and 5, or press enter to exit!");
                    string userInput = Console.ReadLine();

                    if (string.IsNullOrEmpty(userInput)) // if the string object is empty
                    {
                        break; // breaks out of the loop
                    }

                    var number = Convert.ToInt32(userInput);

                    if (number < 0 || number > 5)
                    {
                        Console.WriteLine("Choose a number between 0 and 5");
                        throw new IndexOutOfRangeException(); // if the number is not in the range
                    }
                    else
                        Console.WriteLine("You chose {0}", number);

                }
                catch (IndexOutOfRangeException ex)
                {

                    Console.WriteLine($"Type a number between 0 and 5 {ex.Message}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Characters are not allowed! {ex.ToString()}");
                }
                finally
                {
                    Console.WriteLine("Thank you for trying!!!");
                }
            }
        }
        public static void UsingTryParse()
        {
            // RETURNS TRUE OR FALSE
            // instead of using exception, the TryParse returns true if conversion 
            // from string to number is acheived

            while (true)
            {
                Console.WriteLine("Type a number between 0 and 5, or hit enter to quit");
                string userInput = Console.ReadLine();

                if (string.IsNullOrEmpty(userInput)) break;
                

                if (int.TryParse(userInput, out int result)) // TryParse returns true
                {
                    if(result < 0 || result > 5)
                    {
                        Console.WriteLine("Choose value between 0 and 5");
                        // break;
                    }  
                    else
                    {
                        Console.WriteLine("Conversion successful");
                        Console.WriteLine($"{result} was converted!");
                    }

                }else
                    Console.WriteLine("Conversion failed!");
            }
        }
        public static void Parse()
        {
            // PARSE THROWS TWO ERRORS
            while (true)
            {
                try
                {
                    Console.WriteLine("Type a number between 0 and 5, or hit enter to quit");
                    string userInput = Console.ReadLine();

                    if (string.IsNullOrEmpty(userInput)) break;

                    int value = int.Parse(userInput);
                    // int.Parse throws two exception
                    // 1. OverflowException if a number can be converted but it doesn’t fit
                    // into an int: 

                    // 2. FormatException in case the string passed to this method can’t be
                    // converted to a number

                    if(value < 0 || value > 5)
                    {
                        Console.WriteLine("Choose number between 0 and 5");
                    }
                    else
                    {
                        Console.WriteLine("You chose {0}", value);
                    }

                }
                catch (FormatException ex)
                {

                    Console.WriteLine("Wrong format supplied: " + ex.Message);
                }
                catch (OverflowException ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
