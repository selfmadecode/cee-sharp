﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace practice.Services
{
    class Generics
    {
        //  Generics stores object, the Add method is defned to require an object as a parameter, so an integer type is boxed.
        //  int is a value type while array is a reference type
        /*
            .NET makes it easy to convert value types to reference types, so you can use a
            value type everywhere an object(which is a reference type) is needed.For example, an int can be assigned
            to an object.The conversion from a value type to a reference type is known as boxing.Boxing occurs automatically if a method requires an object as a parameter, and a value type is passed.In the other direction, a
            boxed value type can be converted to a value type by using unboxing.With unboxing, the cast operator is
            required.
        */
        public static void NonGenericAndCast()
        {
            var list = new ArrayList(); // The ArrayList is non Generic
            list.Add(44); // boxing — convert a value type to a reference type
                          // list.Add(new ArrayList() {10, 50 }); //throws an invalid cast exception
                          // int i1 = (int)list[0]; // unboxing — convert a reference type to
                          // a value type using the cast operator

            // list.Add("mystring"); //throws an invalid cast when used with the foreach loop of type int
            // list.Add(new ArrayList()); //throws an invalid cast
            foreach (int i2 in list)
            {
                Console.WriteLine(i2); // unboxing
            }
        }
        public static void GenericWithType()
        {
            // with Generic, a type <T> is specified, and the object accepts on type T
            // the code below accepts ONLY intergers
            var list = new List<int>() { 20, 30, 40 }; // accepts ONLY string
            list.Add(44); // no boxing — value types are stored in the List<int>
            // list.Add("Will not complie"); // the list only accepts int
            int i1 = list[0]; // no unboxing, no cast needed
            foreach (int i2 in list)
            {
                Console.WriteLine(i2);
            }
        }
        public static void Swap<T>(ref T a, ref T b)
        {
            //Generic method to swap items
            T temp; // an empty type that will hold the a, and a will hold b
            temp = a;
            a = b;
            b = temp;
        }
        public static void Dictionary()
        {
            Dictionary<int, string> dObj = new Dictionary<int, string>(5);

            //add elements to Dictionary  
            dObj.Add(0, "Tom");
            dObj.Add(1, "John");
            dObj.Add(2, "Maria");
            dObj.Add(3, "Max");
            dObj.Add(4, "Ram");// Dictionary limit
            dObj.Add(5, "Ram"); // Dictionary expands, same with arrays
            dObj.Add(6, "Ram");

            //for (int i = 0; i < dObj.Count; i++)
            //{
            //    Console.WriteLine(dObj[i]);
            //}

            //print data  
            foreach (var item in dObj)
            {
                Console.WriteLine(item.Value);
            }
        }

        private string Name { get; set; }
        private string Location { get; set; }
        private int Salary { get; set; }

        public Generics(string name, int salary, string location)
        {
            this.Name = name;
            this.Location = location;
            this.Salary = salary;
        }
        public static void AnotherDictionary()
        {
            Dictionary<string, Generics> AllUsers = new Dictionary<string, Generics>();
            Generics Selfmade = new Generics(name: "SelfMade", salary: 120, location: "Ifite");
            AllUsers.Add("SelfMade-Key", Selfmade);
            Generics Obi = new Generics(name: "Obi", salary: 250, location: "Abuja");
            AllUsers.Add("Obi-Key", Obi);
            Generics Chisom = new Generics(name: "Chisom", salary: 150, location: "Nnewi");
            AllUsers.Add("Chisom-Key", Chisom);
            Generics Tk = new Generics(name: "Tochukwu", salary: 150, location: "Cotonou");
            AllUsers.Add("Tk-Key", Tk);

            foreach (var user in AllUsers.Values)
            {
                Console.WriteLine(user.ToString());
            }
        }
        public override string ToString()
        {
            // overide the tostring method to be able to print out data members from a generic collection
            StringBuilder sb = new StringBuilder(200);
            sb.AppendFormat("Your Name is: {0}, {1} is your salary, your live in {2}", Name, Salary, Location);

            return sb.ToString();
        }
        public static void Queue()
        {
            Queue<string> GameList = new Queue<string>();

            GameList.Enqueue("FIFA"); //Enqueue adds an object to the end of the queue
            GameList.Enqueue("PES 19");
            GameList.Enqueue("God of War");
            GameList.Enqueue("Call of Duty");

            while(GameList.Count != 0)
            {
                Console.WriteLine(GameList.Dequeue() + " has been removed!"); // removes the first element and prints the value
            }
            Console.WriteLine("Number of game left in the queue is '{0}'", GameList.Count);
        }



    }
}
