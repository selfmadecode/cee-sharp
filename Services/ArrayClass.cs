﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace practice.Services
{
    class ArrayClass
    {
        static Array myArray = Array.CreateInstance(typeof(int), 3);
        public static void Array1()
        {

            for (int i = 0; i < myArray.Length; i++)
            {
                myArray.SetValue(34, i);
            }

            for (int i = 0; i < myArray.Length; i++)
            {
                Console.WriteLine(myArray.GetValue(i));
            }

            foreach (var item in myArray)
            {
                Console.WriteLine(item);
            }
            
        }
    }
    public class Person
    {
        private string FirstName;
        private string LastName;
        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public Person()
        {

        }
        public static void SetValueOfArray()
        {
            Array racers = Array.CreateInstance(typeof(int), 5);
            racers.SetValue(3, 0); // sets the value 3 at position 0
            racers.SetValue(1, 1);
            racers.SetValue(3, 2);
            racers.SetValue(1, 3);
            racers.SetValue(10, 4);


            for (int i = 0; i < racers.Length; i++)
            {
                // Console.WriteLine(racers.GetValue(i));
            }

            //int[] firstNum = { };
            //firstNum[0] = 1;

            /////////////

            int[] firstarray = new int[4] { 1, 2, 3, 4 };

            int[] secondArray = (int[])firstarray.Clone(); // clone the content of the array into the new
            string[] stringArray = { "Hey", "You", "Welcome", "Ngwa" };


            for (int i = 0; i < secondArray.Length; i++)
            {
                Console.WriteLine(stringArray[i] + " " + secondArray[i]); // arrays must be equal in length
            }

        }
        public static void SortArray()
        {
            string[] names = { "Chidi", "Amara", "ekunife", "Zubeidat", "Amarb" };
            names.Append("Jesus");
            // Array.Sort<string>(names);
            Array.Sort(names);
            // var newArray = names;

            foreach (var name in names)
            {
                // Console.WriteLine(name);
            }

            // a Person Array
            Person[] newPerson =
            {
                new Person("Zubby", "John"),
                new Person("Amk", "aba"),
                new Person("Aamk", "Saba"),
                new Person("Emman", "Rba"),
            };
           //  Array.Sort(newPerson); // inmplement IComparable to use

            //foreach (var person in newPerson)
            //{
            //    Console.WriteLine(person);
            //}

        }
        
    }
}
