﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using practice.Services; // added to us the class in services folder
using static practice.Services.Interface;
//  static practice.Services.Interface;

namespace practice
{
    class Program
    {
        static void NamedArgument(string firstName, string secondName, int age)
        {
            Console.WriteLine($"Hello {firstName} {secondName}, you are {age} years old");
        }
        static void Main(string[] args)
        {
            AsyncAndAwait.CallerWithAsync();
            Console.WriteLine("Me that is here");
            // DELEGATES
            //Delegates MyDelegate = new Delegates(); //An object of the delegate class
           // MyDelegate.UsingDelegate();

           /// Delegates.StartHere();
           // Delegates.MathOperations GetValue = Delegates.Addition;
            // Delegates.MathOperations GetSum = MyDelegate.Sum;

          // Console.WriteLine(GetValue(20, 30));

           // GetValue += Delegates.Multiplication; // multi casting
           // Console.WriteLine(GetValue(20, 30));


            // ARRAYS
            // ArrayClass.Array1();
            // Person.SetValueOfArray();
            // Person.SortArray();

            // ExceptionHandling.UsingTryParse();

            // var calculate = new Operator(1, 2, 3);
            //calculate.Operators();

            // an object with different datatype, using the OfType returns a specific data type

            // object[] data = { "Tocukwu", "Hank", 21, 'A', true };
            // var filtered = data.OfType<string>(); // return only the strings in the object array

            //foreach (var item in filtered)
            //{
            //    Console.WriteLine(item);
            //}
            // USING GENERICS
            // Generics.NonGenericAndCast();
            // Generics.Dictionary();
            // Generics.AnotherDictionary();
            // Generics.Queue();

            // Swap of two integers.  
            // int a = 40, b = 60;
            // Console.WriteLine("Before swap: {0}, {1}", a, b);

            // Generics.Swap<int>(ref a, ref b);

            // Console.WriteLine("After swap: {0}, {1}", a, b);


            //INTERFACE Implementation

            /*
            var firstBankAccount = new Interface.FirstBank();
            // var firstBankAccount = new FirstBank();
            //To use inner class member, add the base class to the namespace(uncomment line 4)
            //you can call only methods that are part of this interface through these references
            firstBankAccount.PayIn(150);
            firstBankAccount.CheckBalance();
            //firstBankAccount.WithDraw(300);
            firstBankAccount.WithDraw(100);
            firstBankAccount.CheckBalance();
            // the string class was overide
            Console.WriteLine(firstBankAccount.ToString());
            //  IBankAccount sav = new FirstBank(); // uncomment line 4 to use this

            ITransferBankAccount diamondBankAccount = new DiamondBank();

            diamondBankAccount.PayIn(1000);
            diamondBankAccount.CheckBalance();
            diamondBankAccount.TransferTo(firstBankAccount, 950); // takes an IBankAccount type as params
            
            firstBankAccount.CheckBalance();
            diamondBankAccount.CheckBalance();

            */

            //USING DERIVED CLASSES
            /*
            var a = new DerivedClass.NonAbstarctClass();
            a.Resize(@"Hello, this class derives from the Abstarct Class 'AbstractShape'
1. An abstract class cannot be instantiated
2. An Class that derives from an abstract class muat implement all its Abstract methods
3. An Abstract class can contain non-abstract methods.
4. An abstract class can have implementations or abstract members without implementation
");

            */

            /*
            //////////////////////////////
            var r = new DerivedClass.Rectangle();
            r.position.x = 33;
            r.position.y = 22;
            r.size.Width = 200;
            r.size.Height = 100;
            r.Draw();
            */


            // USING CONSTRUCTORS

            /*
                ConstructorClass newCon = new ConstructorClass(10, 20); // initializes the constructor with two values
                newCon.GetDiagonal();
            */

            //OPTIONAL ARGUMENTS and Named params

            /*
                OptionalArgument.OptionalArg(notOptionalArgument: "SelfMade have been passed as the first argument, ");
                OptionalArgument.OptionalArg(notOptionalArgument: "Selfmade", optionalArgument: "Deafult second Argument have been overwritten"); // the second argument have
            */

            //Passing any number of parameter

            /*
                OptionalArgument.VariableNumberOfParam(10, 20, 30, 40, 50, 60, 70, 80, 90, 100);
                OptionalArgument.VariableNumberOfParam(10, '%', "Hey", 4);
            */

            // using named argument
            NamedArgument(firstName: "Anyanwu", secondName: "Raphael", age: 10 );


            // not good to make fields public, you can overide its data

            /*
            fields.getAge(12); // assigning values to a private field using a property and method
            fields.ChangeUserName("SelfMade ", "Anyanwu");
            fields.ChangeUserName("Anyanwu ", "Raphael", age: 30); // using named argument
            // fields.FirstName = "Ojo"; // the firstname is private
            fields.getNames(); // notice that the firstname and lastname has been changed
            */


            /*
            // I do not need to initialization for static types in the random number class
            // uncomment lines with 3 slashes
            RandomNumber.GenRandNum(1, 20);

            // to use non-static type, an instance must be created
             var RandNm = new RandomNumber();
             RandNm.GenRandNum2(1, 20);
            */

            /*
            DateTimeClass.myName = "ANYANWU RAPHAEL"; // sending data to a class
            // Gets tomorrow and yesterday's date
              DateTimeClass.TomorrowAndYesterday();
            // gets the first and last day of a given month
              DateTimeClass.FirstAndLastDay();
            */

            // Console.WriteLine("First Value" + Environment.NewLine + "second Value");
            //  Console.WriteLine("If the font knows, here is Greek beta: \u03B2");

            Console.Title = "Practice";
            Console.ReadLine();
        }
        public static void TryHandling()
        {
            // Input of 1. number
            try
            {
                Console.Write("Enter 1. number: ");
                string input1 = Console.ReadLine();
                int number1 = Convert.ToInt32(input1);
                // Input of 2. number
                Console.Write("Enter 2. number: ");
                string input2 = Console.ReadLine();
                int number2 = Convert.ToInt32(input2);
                // Calculating
                int result = number1 + number2;
                // Result output
                Console.WriteLine("Sum of entered numbers is: " + result);
            }
            catch (Exception)
            {

                Console.WriteLine("Incorect details");
            }
        }

        static void WhatIs(string name) => Console.WriteLine(name);
        

        static void collections()
        {
            //COLLECTIONS
            var mylist = new List<string> { "ff", "son" };
            mylist.Add("God");

            for (int i = 0; i < mylist.Count; i++)
            {
                Console.WriteLine(mylist[i]);
            }
            foreach (var list in mylist)
            {
                Console.WriteLine(list);
            }
        }

        static void EnvObj()
        {
            Console.WriteLine("Device name: " + Environment.MachineName);
            Console.WriteLine("64-bit system: " + Environment.Is64BitOperatingSystem);
            Console.WriteLine("User name: " + Environment.UserName);
            // Waiting for Enter
            Console.ReadLine();
        }
        
    }
}
